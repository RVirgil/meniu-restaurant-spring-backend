import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
public class Fel3Controller {
    @Autowired
    Fel3Repo fel3Repo;
    @GetMapping("/fel3")
    public List<Fel3> GetFel3(){
        return fel3Repo.findAll();
    }

    @PostMapping("/createFel3")
    public Fel3 MakeFel3(@PathVariable String nume){
        Fel3 fel3=new Fel3(nume);
        fel3Repo.save(fel3);
        return fel3;

    }

    @GetMapping("/fel3/get/{id_fel3}")
    public Fel3 GetFel3ById(@PathVariable(value="id_fel3")long id_fel3) throws Fel3NotFoundException{
        return fel3Repo.findById(id_fel3).orElseThrow(()->new Fel3NotFoundException(id_fel3));
    }
    // Update a Note
    @PutMapping("/fel3/update/{id_fel3}")
    public Fel3 UpdateFel3(@PathVariable(value = "id_fel3") long id_fel3, @PathVariable String name) throws Fel3NotFoundException {

        Fel3 fel3 = fel3Repo.findById(id_fel3).orElseThrow(() -> new Fel3NotFoundException(id_fel3));

        fel3.name=name;

        return fel3Repo.save(fel3);
    }

    @DeleteMapping("/fel3/delete/{id_fel3}")
    public ResponseEntity<?> DeleteFel3(@PathVariable(value = "id_fel3") Long id_fel3) throws Fel3NotFoundException {
        fel3Repo.delete( fel3Repo.findById(id_fel3).orElseThrow(() -> new Fel3NotFoundException(id_fel3)));
        return ResponseEntity.ok().build();
    }
}
