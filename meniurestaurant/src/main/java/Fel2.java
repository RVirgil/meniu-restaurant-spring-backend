import javax.persistence.*;

@Entity
public class Fel2 {
    @Id
    @GeneratedValue
    @Column(name="id_fel2")
    public long id_fel2;

    @Column(name="name")
    public String name;

    @OneToOne
    private Meniu meniu;

    public Fel2(){}
    public Fel2(String name){this.name=name;}

    @Override
    public String toString(){
        return name+" ";
    }


}