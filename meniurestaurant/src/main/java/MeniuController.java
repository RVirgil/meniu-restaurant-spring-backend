import net.minidev.json.JSONUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class MeniuController {

    @Autowired
        MeniuRepo meniuRepo;

    @GetMapping("/meniu")
    public List<Meniu> GetMeniu(){
        return meniuRepo.findAll();
    }

    @PostMapping("/createMeniu")
    public Meniu MakeMeniu(@PathVariable("id_fel1")long id_fel1,@PathVariable("id_fel2")long id_fel2,@PathVariable("id_fel3")long id_fel3,@PathVariable("id_dessert")long id_dessert){
        Meniu meniu=new Meniu(id_fel1,id_fel2,id_fel3,id_dessert);
        meniuRepo.save(meniu);
        return meniu;

    }

    @GetMapping("/meniu/get/{id_meniu}")
    public Meniu GetMeniuById(@PathVariable(value="id_meniu")long id_meniu) throws MeniuNotFoundException{
        return meniuRepo.findById(id_meniu).orElseThrow(()->new MeniuNotFoundException(id_meniu));
    }
    // Update a Note
    @PutMapping("/meniu/update/{id_meniu}")
    public Meniu UpdateMeniu(@PathVariable(value = "id_meniu") Long id_meniu, @Valid @RequestBody Meniu meniuDetails) throws MeniuNotFoundException {

        Meniu meniu = meniuRepo.findById(id_meniu).orElseThrow(() -> new MeniuNotFoundException(id_meniu));

        meniu.fel1=meniuDetails.fel1;
        meniu.fel2=meniuDetails.fel2;
        meniu.fel3=meniuDetails.fel3;
        meniu.dessert=meniuDetails.dessert;

        return meniuRepo.save(meniu);
    }

    @DeleteMapping("/meniu/delete/{id_meniu}")
    public ResponseEntity<?> DeleteMeniu(@PathVariable(value = "id_meniu") Long id_meniu) throws MeniuNotFoundException {
        meniuRepo.delete( meniuRepo.findById(id_meniu).orElseThrow(() -> new MeniuNotFoundException(id_meniu)));
        return ResponseEntity.ok().build();
    }



}
