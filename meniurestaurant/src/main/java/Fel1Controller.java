import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
public class Fel1Controller {
    @Autowired
        Fel1Repo fel1Repo;
    @GetMapping("/fel1")
    public List<Fel1> GetFel1(){
        return fel1Repo.findAll();
    }

    @PostMapping("/createFel1")
    public Fel1 MakeFel1(@PathVariable String nume){
        Fel1 fel1=new Fel1(nume);
        fel1Repo.save(fel1);
        return fel1;

    }

    @GetMapping("/fel1/get/{id_fel1}")
    public Fel1 GetFel1ById(@PathVariable(value="id_fel1")long id_fel1) throws Fel1NotFoundException{
        return fel1Repo.findById(id_fel1).orElseThrow(()->new Fel1NotFoundException(id_fel1));
    }
    // Update a Note
    @PutMapping("/fel1/update/{id_fel1}")
    public Fel1 UpdateFel1(@PathVariable(value = "id_fel1") long id_fel1, @PathVariable String name) throws Fel1NotFoundException {

        Fel1 fel1 = fel1Repo.findById(id_fel1).orElseThrow(() -> new Fel1NotFoundException(id_fel1));

        fel1.name=name;

        return fel1Repo.save(fel1);
    }

    @DeleteMapping("/fel1/delete/{id_fel1}")
    public ResponseEntity<?> DeleteFel1(@PathVariable(value = "id_fel1") Long id_fel1) throws Fel1NotFoundException {
        fel1Repo.delete( fel1Repo.findById(id_fel1).orElseThrow(() -> new Fel1NotFoundException(id_fel1)));
        return ResponseEntity.ok().build();
    }
}
