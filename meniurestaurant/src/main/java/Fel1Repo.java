import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Fel1Repo extends JpaRepository<Fel1, Long> {
}
