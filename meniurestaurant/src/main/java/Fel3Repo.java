import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Fel3Repo extends JpaRepository<Fel3, Long> {
}
