import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Fel2Repo extends JpaRepository<Fel2, Long> {
}
