public class DessertNotFoundException extends Exception {
    private long id_dessert;
    public DessertNotFoundException(long id_dessert){
        super("Dessert with id "+id_dessert+" is not found");
    }
}
