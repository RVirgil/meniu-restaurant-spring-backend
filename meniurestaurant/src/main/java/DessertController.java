import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
public class DessertController {
    @Autowired
    DessertRepo dessertRepo;
    @GetMapping("/dessert")
    public List<Dessert> GetDessert(){
        return dessertRepo.findAll();
    }


    @PostMapping("/createDessert")
    public Dessert MakeDessert(@PathVariable String nume){
        Dessert dessert=new Dessert(nume);
        dessertRepo.save(dessert);
        return dessert;

    }

    @GetMapping("/dessert/get/{id_dessert}")
    public Dessert GetDessertById(@PathVariable(value="id_dessert")long id_dessert) throws DessertNotFoundException{
        return dessertRepo.findById(id_dessert).orElseThrow(()->new DessertNotFoundException(id_dessert));
    }
    // Update a Note
    @PutMapping("/Dessert/update/{id_dessert}")
    public Dessert UpdateDessert(@PathVariable(value = "id_dessert") long id_dessert, @PathVariable String name) throws DessertNotFoundException {

        Dessert dessert = dessertRepo.findById(id_dessert).orElseThrow(() -> new DessertNotFoundException(id_dessert));

        dessert.name=name;

        return dessertRepo.save(dessert);
    }

    @DeleteMapping("/dessert/delete/{id_dessert}")
    public ResponseEntity<?> DeleteDessert(@PathVariable(value = "id_dessert") Long id_dessert) throws DessertNotFoundException {
        dessertRepo.delete( dessertRepo.findById(id_dessert).orElseThrow(() -> new DessertNotFoundException(id_dessert)));
        return ResponseEntity.ok().build();
    }
}
