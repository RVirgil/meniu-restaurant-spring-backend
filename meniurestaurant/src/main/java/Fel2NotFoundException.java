public class Fel2NotFoundException extends Exception {
    private long id_fel2;
    public Fel2NotFoundException(long id_fel2){
        super("Fel2 with id "+id_fel2+" is not found");
    }
}
