import javax.persistence.*;

@Entity
public class Dessert {
    @Id
    @GeneratedValue
    @Column(name="id_dessert")
    public long id_dessert;

    @Column(name="name")
    public String name;

    @OneToOne
    private Meniu meniu;

    public Dessert(){}
    public Dessert(String name){this.name=name;}

    @Override
    public String toString(){
        return name+" ";
    }


}