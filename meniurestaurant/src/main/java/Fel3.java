import javax.persistence.*;

@Entity
public class Fel3 {
    @Id
    @GeneratedValue
    @Column(name="id_fel3")
    public long id_fel3;

    @Column(name="name")
    public String name;

    @OneToOne
    private Meniu meniu;

    public Fel3(){}
    public Fel3(String name){this.name=name;}

    @Override
    public String toString(){
        return name+" ";
    }

}