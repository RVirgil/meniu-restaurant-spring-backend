import javax.persistence.*;

@Entity
public class Meniu {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    public long id_meniu;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="id_fel1")
    public Fel1 fel1;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="id_fel2")
    public Fel1 fel2;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="id_fel3")
    public Fel1 fel3;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="id_dessert")
    public Fel1 dessert;

    public Meniu(){}
    public Meniu(long id_fel1,long id_fel2,long id_fel3,long id_dessert){
        fel1.id_fel1=id_fel1;
        fel2.id_fel1=id_fel2;
        fel3.id_fel1=id_fel3;
        dessert.id_fel1=id_dessert;
    }

    @Override
    public String toString(){
        return "Meniul de azi: "+fel1.name+", "+fel2.name+", "+fel3.name+", "+dessert.name;
    }






}
