import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
public class Fel2Controller {
    @Autowired
    Fel2Repo fel2Repo;
    @GetMapping("/fel2")
    public List<Fel2> GetFel2(){
        return fel2Repo.findAll();
    }

    @PostMapping("/createFel2")
    public Fel2 MakeFel2(@PathVariable String nume){
        Fel2 fel2=new Fel2(nume);
        fel2Repo.save(fel2);
        return fel2;

    }

    @GetMapping("/fel2/get/{id_fel2}")
    public Fel2 GetFel2ById(@PathVariable(value="id_fel2")long id_fel2) throws Fel2NotFoundException{
        return fel2Repo.findById(id_fel2).orElseThrow(()->new Fel2NotFoundException(id_fel2));
    }

    @PutMapping("/fel2/update/{id_fel2}")
    public Fel2 UpdateFel2(@PathVariable(value = "id_fel2") long id_fel2, @PathVariable String name) throws Fel2NotFoundException {

        Fel2 fel2 = fel2Repo.findById(id_fel2).orElseThrow(() -> new Fel2NotFoundException(id_fel2));

        fel2.name=name;

        return fel2Repo.save(fel2);
    }

    @DeleteMapping("/fel2/delete/{id_fel2}")
    public ResponseEntity<?> DeleteFel2(@PathVariable(value = "id_fel2") Long id_fel2) throws Fel2NotFoundException {
        fel2Repo.delete( fel2Repo.findById(id_fel2).orElseThrow(() -> new Fel2NotFoundException(id_fel2)));
        return ResponseEntity.ok().build();
    }
}
