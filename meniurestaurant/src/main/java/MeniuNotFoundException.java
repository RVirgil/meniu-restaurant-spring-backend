public class MeniuNotFoundException extends Exception {
    private long id_meniu;
    public MeniuNotFoundException(long id_meniu){
        super("Meniu with id "+id_meniu+" is not found");
    }
}
