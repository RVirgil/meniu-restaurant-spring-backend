public class Fel1NotFoundException extends Exception {
    private long id_fel1;
    public Fel1NotFoundException(long id_fel1){
        super("Fel1 with id "+id_fel1+" is not found");
    }
}
