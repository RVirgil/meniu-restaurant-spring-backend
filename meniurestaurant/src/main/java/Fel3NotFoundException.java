public class Fel3NotFoundException extends Exception {
    private long id_fel3;
    public Fel3NotFoundException(long id_fel3){
        super("Fel3 with id "+id_fel3+" is not found");
    }
}
