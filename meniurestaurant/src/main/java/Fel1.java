import javax.persistence.*;

@Entity
public class Fel1 {
    @Id
    @GeneratedValue
    @Column(name="id_fel1")
    public long id_fel1;

    @Column(name="name")
    public String name;

    @OneToOne
    private Meniu meniu;

    public Fel1(){}
    public Fel1(String name){this.name=name;}

    @Override
    public String toString(){
        return name+" ";
    }


}
